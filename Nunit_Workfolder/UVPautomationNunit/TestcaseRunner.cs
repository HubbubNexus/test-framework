﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using UVPautomationNunit.Uitilityclass;
using UVPautomationNunit.Pageobjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace UVPautomationNunit
{
    [TestFixture]
    class TestcaseRunner : utilityclass
    {
        [Test,Order(1)]
        [Description("To Check the URL open's the first step")]
        public void Checkstep1headername()
        {
            var uvpstep1 = new uvpstep1(driver);

            if (uvpstep1.checkstep1header())
            {
                Assert.Pass("Step1 wizard opened Successfully");
            }
            else
            {
                Assert.Fail("Step1 wizard Not opened");
                Exitbrowser();
            }

        }

        [Test,Order(2)]
        [Description("Check drop down passenger vehicle")]
        public void passenger_vechile()
        {
            var uvpstep1 = new uvpstep1(driver);

            uvpstep1.vehicletype_();

            Assert.AreEqual("Sedan", uvpstep1.vehicletype_sub_list.GetAttribute("value"));

        }

        [Test, Order(3)]
        [Description("Check drop down Prime Mover")]
        public void prime_mover()
        {
            var uvpstep1 = new uvpstep1(driver);

            uvpstep1.vehicletype_primemover();

            Assert.False(uvpstep1.vehicletype_sub_list.Displayed);
        }

        [Test, Order(4)]
        [Description("Check invalid address")]
        public void Checkaddress()
        {
            var uvpstep1 = new uvpstep1(driver);

            Assert.AreEqual("The address must end in a valid Australian postcode.", uvpstep1.invalid_address());
            Thread.Sleep(5000);

            uvpstep1.valid_address();

        }

        [Test, Order(5)]
        [Description("Check invalid date")]
        public void checkdate()
        {
            var uvpstep1 = new uvpstep1(driver);

            Assert.AreEqual("Please enter the date in dd/mm/yyyy format.", uvpstep1.invalid_date());
            Thread.Sleep(5000);
            uvpstep1.valid_date();

        }


        [Test, Order(6)]
        [Description("Check enddate")]
        public void Check_enddate()
        {
            var uvpstep1 = new uvpstep1(driver);

            Assert.AreEqual("5 September 2021", uvpstep1.checkpermitenddate());
        }

        [Test, Order(7)]
        [Description("Check enddate")]
        public void Checkcalculation()
        {
            var uvpstep1 = new uvpstep1(driver);

            uvpstep1.click_calc();

            Assert.AreEqual("Your permit will cost:", uvpstep1.calc_result.Text);

            uvpstep1.nextbtn.Click();
            Thread.Sleep(5000);
        }


        [Test, Order(8)]
        [Description("Check step2 name display")]
        public void Checkstep2name()
        {
            var uvpstep2 = new uvpstep2(driver);

            Assert.IsTrue(uvpstep2.checknamedisplay().Contains("Step 2 of 7 : Select permit type"));
        }

    }
}
