﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace UVPautomationNunit.Uitilityclass
{
    class utilityclass
    {
        public IWebDriver driver;

        [OneTimeSetUp]
        public void Initbrowser()
        {
            try
            {
                driver = new ChromeDriver();
                driver.Url = "https://www.vicroads.vic.gov.au/registration/limited-use-permits/unregistered-vehicle-permits/get-an-unregistered-vehicle-permit";
                driver.Manage().Window.Maximize();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                throw;
            }
            
        }

        [OneTimeTearDown]
        public void Exitbrowser()
        {
            driver.Quit();
        }
    }
}
