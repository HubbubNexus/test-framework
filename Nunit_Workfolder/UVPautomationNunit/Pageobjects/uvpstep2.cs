﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;


namespace UVPautomationNunit.Pageobjects
{
    class uvpstep2
    {
        public IWebDriver driver;
        public uvpstep2(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        //Check step2 name
        [FindsBy(How = How.XPath, Using = "//*[@id='main']/div/p")]
        public IWebElement step2name {get;set;}


        public string checknamedisplay()
        {
            return step2name.Text;
        }
    }
}
