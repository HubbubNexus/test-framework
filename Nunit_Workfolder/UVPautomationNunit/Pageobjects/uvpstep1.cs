﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace UVPautomationNunit.Pageobjects
{
    class uvpstep1
    {
        public IWebDriver driver;
        public uvpstep1(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver,this);
        }

        //Step1 wizard header name
        [FindsBy(How = How.XPath,Using = "//*[@id='main']/div/p")]
        public IWebElement step1headername { get; set; }

        //Vehicle Type
        [FindsBy(How = How.Id,Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_VehicleType_DDList")]
        public IWebElement vehicletype { get; set; }

        //Vehicle Type label
        [FindsBy(How = How.XPath,Using = "//*[@id='ph_pagebody_0_phthreecolumnmaincontent_0_panel_PassengerVehicleSubType_CtrlHolder']/label")]
        public IWebElement vehicletypelabel { get; set; }

        //Vehicle Type sub list
        [FindsBy(How = How.Id,Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_PassengerVehicleSubType_DDList")]
        public IWebElement vehicletype_sub_list { get; set; }

        //Address
        [FindsBy(How = How.Id,Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_AddressLine_SingleLine_CtrlHolderDivShown")]
        public IWebElement Address { get; set; }

        //address error
        [FindsBy(How =How.Id,Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_AddressLine_SingleLine_CtrlHolderDivShown-error")]
        public IWebElement addressmsg { get; set; }

        //Date
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitStartDate_Date")]
        public IWebElement Date { get; set; }

        //Date error
        [FindsBy(How= How.Id,Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitStartDate_Date-error")]
        public IWebElement date_error { get; set; }

        //Permit Duration
        [FindsBy(How = How.Id,Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitDuration_DDList")]
        public IWebElement permitduration { get; set; }

        //Permit Duration label
        [FindsBy(How = How.XPath,Using = "//*[@id='PermitDurationModule']/span[2]")]
        public IWebElement permitenddate { get; set; }

        //Error Summary list
        [FindsBy(How = How.XPath, Using = "//*[@id='ph_pagebody_0_phthreecolumnmaincontent_0_panel_ErrorSummary']/ul")]
        public IWebElement Errorlist { get; set; }

        //Calculate Button
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnCal")]
        public IWebElement calculatebtn { get; set; }

        //Calculate result
        [FindsBy(How= How.XPath, Using = "//*[@id='ph_pagebody_0_phthreecolumnmaincontent_0_panel_divfee']/span[1]")]
        public IWebElement calc_result { get; set; }

        //Next Button
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext")]
        public IWebElement nextbtn { get; set; }


        public bool checkstep1header()
        {
            //Thread.Sleep(5000);
            return step1headername.Displayed;
        }

        //Check the mandatory fields
        public IList<IWebElement> mandatorycheck()
        {
            calculatebtn.Click();

            List<IWebElement> validation = Errorlist.FindElements(By.TagName("li")).ToList();

            return validation;
        }


        //Check vehicle type dropdown
        public void vehicletype_()
        {
            SelectElement element = new SelectElement(vehicletype);
            element.SelectByText("Passenger vehicle");

            Thread.Sleep(5000);

            SelectElement element_sub = new SelectElement(vehicletype_sub_list);
            element_sub.SelectByText("Sedan");
        }

        //Check vehicle type prime mover
        public void vehicletype_primemover()
        {
            SelectElement element = new SelectElement(vehicletype);
            element.SelectByText("Prime Mover");
            
        }

        //check invalid address 
        public string invalid_address()
        {
            Address.SendKeys("Unit 3 11 Broadmeadows Rd, TULLAMARINE VIC 304");
            Address.SendKeys(Keys.Tab);

            Thread.Sleep(5000);

            return addressmsg.Text;

        }

        public void valid_address()
        {
            Address.Clear();
            Address.SendKeys("Unit 3 11 Bradsha St, KINGSBURY VIC 3083");
            Address.SendKeys(Keys.Tab);
        }

        //Check Invalid date
        public string invalid_date()
        {
            Date.Clear();
            Date.SendKeys("01/9/2021");
            Date.SendKeys(Keys.Tab);
            Thread.Sleep(5000);

            return date_error.Text;
        }

        public void valid_date()
        {
            Date.Clear();
            Date.SendKeys("01/09/2021");
        }

        public string checkpermitenddate()
        {
            SelectElement end_date = new SelectElement(permitduration);
            end_date.SelectByText("5 days");

            Thread.Sleep(5000);
            return permitenddate.Text;

        }

        //Check calculate
        public void click_calc()
        {
            calculatebtn.Click();
            Thread.Sleep(5000);
        }


    }
}
