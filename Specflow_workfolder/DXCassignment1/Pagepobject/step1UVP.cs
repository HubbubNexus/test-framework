﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace DXCassignment1.Pagepobject
{
    class step1UVP
    {
        IWebDriver driver;

        public step1UVP(IWebDriver driver)
        {
            this.driver = driver;
        }

        //Step1 wizard header name
        [FindsBy(How = How.XPath, Using = "//*[@id='main']/div/p")]
        public IWebElement step1headername { get; set; }

        //Vehicle Type
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_VehicleType_DDList")]
        public IWebElement vehicletype { get; set; }

        //Vehicle Type label
        [FindsBy(How = How.XPath, Using = "//*[@id='ph_pagebody_0_phthreecolumnmaincontent_0_panel_PassengerVehicleSubType_CtrlHolder']/label")]
        public IWebElement vehicletypelabel { get; set; }

        //Vehicle Type sub list
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_PassengerVehicleSubType_DDList")]
        public IWebElement vehicletype_sub_list { get; set; }

        //Address
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_AddressLine_SingleLine_CtrlHolderDivShown")]
        public IWebElement Address { get; set; }

        //address error
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_AddressLine_SingleLine_CtrlHolderDivShown-error")]
        public IWebElement addressmsg { get; set; }

        //Date
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitStartDate_Date")]
        public IWebElement Date { get; set; }

        //Date error
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitStartDate_Date-error")]
        public IWebElement date_error { get; set; }

        //Permit Duration
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitDuration_DDList")]
        public IWebElement permitduration { get; set; }

        //Permit Duration label
        [FindsBy(How = How.XPath, Using = "//*[@id='PermitDurationModule']/span[2]")]
        public IWebElement permitenddate { get; set; }

        //Error Summary list
        [FindsBy(How = How.XPath, Using = "//*[@id='ph_pagebody_0_phthreecolumnmaincontent_0_panel_ErrorSummary']/ul")]
        public IWebElement Errorlist { get; set; }

        //Calculate Button
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnCal")]
        public IWebElement calculatebtn { get; set; }

        //Calculate result
        [FindsBy(How = How.XPath, Using = "//*[@id='ph_pagebody_0_phthreecolumnmaincontent_0_panel_divfee']/span[1]")]
        public IWebElement calc_result { get; set; }

        //Next Button
        [FindsBy(How = How.Id, Using = "ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext")]
        public IWebElement nextbtn { get; set; }

    }
}
