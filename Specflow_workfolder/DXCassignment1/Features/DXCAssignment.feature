﻿Feature: DXCAssignment
	Test UVP application form Step1 and Step2 in chrome browser

@Step1ofUVP
Scenario: Enter all details in step1 and calculate
	Given navigate to UVP url
	And Choose the vehicle type
	And Choose the vehicle details
	And Enter the Address
	And Enter the date
	And Choose the permit duration days
	When Now click the calculate button
	And Verify the calculated amount
	And CLick Next button
	Then Verify the step2 header name
	
	
