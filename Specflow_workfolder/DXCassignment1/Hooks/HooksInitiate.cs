﻿using DXCassignment1.Drivers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace DXCassignment1.Hooks
{
    [Binding]
    public sealed class HooksInitiate
    {
        // For additional details on SpecFlow hooks see http://go.specflow.org/doc-hooks

        private readonly ScenarioContext _ScenarioContext;

        public HooksInitiate(ScenarioContext scontext)
        {
            _ScenarioContext = scontext;
        }
        
        [BeforeScenario]
        public void BeforeScenario()
        {
            Uitilitydrivers udriver = new Uitilitydrivers(_ScenarioContext);
            _ScenarioContext.Set(udriver, "Uitilitydrivers");
        }

        [AfterScenario]
        public void AfterScenario()
        {
            _ScenarioContext.Get<IWebDriver>("WebDriver").Quit();
        }
    }
}
