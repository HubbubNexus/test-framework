﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace DXCassignment1.Drivers
{
    class Uitilitydrivers
    {
        public IWebDriver driver;

        private ScenarioContext _scenarioContext;

        public Uitilitydrivers(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        public IWebDriver Setup()
        {
            var driver = new ChromeDriver();

            //driver.Url = "https://www.vicroads.vic.gov.au/registration/limited-use-permits/unregistered-vehicle-permits/get-an-unregistered-vehicle-permit";

            driver.Manage().Window.Maximize();

            _scenarioContext.Set(driver, "WebDriver");

            return driver;
        }
    }
}
