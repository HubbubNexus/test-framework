﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using DXCassignment1.Drivers;
using DXCassignment1.Pagepobject;
using System.Threading;
using NUnit.Framework;

namespace DXCassignment1.Steps
{
    [Binding]
    public sealed class Teststepdefinition
    {
        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef

        public IWebDriver driver;

        private readonly ScenarioContext _scenarioContext;

        public Teststepdefinition(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given(@"navigate to UVP url")]
        public void GivenNavigateToUVPUrl()
        {
            driver = _scenarioContext.Get<Uitilitydrivers>("Uitilitydrivers").Setup();
            driver.Url = "https://www.vicroads.vic.gov.au/registration/limited-use-permits/unregistered-vehicle-permits/get-an-unregistered-vehicle-permit";

        }

        [Given(@"Choose the vehicle type")]
        public void GivenChooseTheVehicleType()
        {
            
            IWebElement type = driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_VehicleType_DDList"));
            SelectElement vtype = new SelectElement(type);
            vtype.SelectByText("Passenger vehicle");
           
        }

        [Given(@"Choose the vehicle details")]
        public void GivenChooseTheVehicleDetails()
        {
            IWebElement sub_type = driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_PassengerVehicleSubType_DDList"));
            SelectElement sub_vtype = new SelectElement(sub_type);
            sub_vtype.SelectByText("Sedan");
            
        }

        [Given(@"Enter the Address")]
        public void GivenEnterTheAddress()
        {
            IWebElement Address = driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_AddressLine_SingleLine_CtrlHolderDivShown"));
            Address.SendKeys("Unit 3 11 Bradsha St, KINGSBURY VIC 3083");
            Address.SendKeys(Keys.Tab);
        }

        [Given(@"Enter the date")]
        public void GivenEnterTheDate()
        {
            IWebElement date = driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitStartDate_Date"));
            date.Clear();
            date.SendKeys("01/09/2021");
        }

        [Given(@"Choose the permit duration days")]
        public void GivenChooseThePermitDurationDays()
        {
            IWebElement permitduration = driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitDuration_DDList"));
            SelectElement end_date = new SelectElement(permitduration);
            end_date.SelectByText("5 days");

        }

        [When(@"Now click the calculate button")]
        public void WhenNowClickTheCalculateButton()
        {
            driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnCal")).Click();
            Thread.Sleep(5000);
        }


        [When(@"Verify the calculated amount")]
        public void WhenVerifyTheCalculatedAmount()
        {
            var get_result = driver.FindElement(By.XPath("//*[@id='ph_pagebody_0_phthreecolumnmaincontent_0_panel_divfee']/span[1]")).Text;

            Thread.Sleep(5000);
            Assert.AreEqual("Your permit will cost:", get_result);
        }

        [When(@"CLick Next button")]
        public void WhenCLickNextButton()
        {
            driver.FindElement(By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext")).Click();
            Thread.Sleep(5000);
        }

        [Then(@"Verify the step(.*) header name")]
        public void ThenVerifyTheStepHeaderName(int p0)
        {
            var get_text = driver.FindElement(By.XPath("//*[@id='main']/div/p")).Text;

            Assert.IsTrue(get_text.Contains("Step 2 of 7 : Select permit type"));
        }


    }
}
