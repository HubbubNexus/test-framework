Description:
To automate UVP portal step1 and step2.

Chrome driver version: 85.0

Included frameworks:
1. Nunit with page object model.
2. Specflow.

Nunit framework:
1.Verified dropdown fields.
2.Verified Address format.
3.Verified date format.
4.Verified number of days and amount.
5.Checked step2 header name.
6.Created a separate class for pom and listed all page objects.


Specflow:
Completely a new framework for me. i went through few examples and setup the basic flow of automation.
1. created a feature file.
2. created a description file.
3. created hooks to initate drivers.
4. Verified the step1 and step2 pages.

Sorry unable to compile as a executable file. have uploaded the sln files,
it can be downloaded and executed in visual studio for now. 



